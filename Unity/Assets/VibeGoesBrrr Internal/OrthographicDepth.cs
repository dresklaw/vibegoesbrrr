﻿using UnityEngine;
using UnityEngine.Rendering;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Collections;
#if !UNITY_STANDALONE
using System;
#endif

namespace VibeGoesBrrr
{
#if UNITY_STANDALONE
  [ExecuteInEditMode]
#endif
  public class OrthographicDepth : MonoBehaviour
  {
    public float Depth = 0f;

    private Camera mCamera;
    private AsyncGPUReadbackRequest? mReadRequest;

    public void SetShader(Shader shader)
    {
      mCamera.SetReplacementShader(shader, null);
    }

    void Awake()
    {
      mCamera = GetComponent<Camera>();
      mCamera.clearFlags = CameraClearFlags.SolidColor;
      mCamera.cullingMask = 394240; // CVR: PlayerNetwork | CVRPickup | CVRInteractable
      mCamera.backgroundColor = Color.black;
      mCamera.targetTexture = new RenderTexture(48, 48, 0, RenderTextureFormat.R8);
      mCamera.depthTextureMode = DepthTextureMode.None;
      mCamera.SetReplacementShader(Shader.Find("Orthographic Depth"), null);
    }

    void OnDisable()
    {
      Depth = 0f;
    }

    void Update()
    {
      if (mReadRequest == null) {
        mReadRequest = AsyncGPUReadback.Request(mCamera.targetTexture);
      }

      var request = (AsyncGPUReadbackRequest)mReadRequest;
      if (request.done) {
        try {
          var texture = new Texture2D(mCamera.targetTexture.width, mCamera.targetTexture.height, TextureFormat.R8, false);
// #if UNITY_STANDALONE
          var data = request.GetData<float>();
          texture.LoadRawTextureData(data);
// #else
//           var data = request.GetDataRaw(0);
//           texture.LoadRawTextureData(data, request.layerDataSize);
// #endif

          //var pixels = BitConverter.texture.GetRawTextureData();
          //var pixels = NativeArrayUnsafeUtility.ConvertExistingDataToNativeArray<float>((void*)texture.GetWritableImageData(0), (int)(texture.GetRawImageDataSize() / UnsafeUtility.SizeOf<float>()), Allocator.None);
          var pixels = texture.GetPixels();
          var n = 0;
          var closeness = 0f;
          foreach (var p in pixels) {
            if (p.r > 0) {
              closeness += p.r;
              n++;
            }
          }
          closeness = n > 0 ? closeness / n : 0;
          var area = n / pixels.Length;

          Depth = closeness;

          mReadRequest = null;
        } catch (System.InvalidOperationException) {
          mReadRequest = null;
        }
      }
    }
  }
}
