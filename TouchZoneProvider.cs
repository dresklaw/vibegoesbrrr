using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using ABI.CCK.Components;
using UnityEngine;

namespace VibeGoesBrrr
{
  class TouchZoneProvider : ISensorProvider, IDisposable
  {
    public class TouchSensor : Sensor
    {
      public static Regex Pattern = new Regex(@"^\s*?(Touch\s*?Zone|Vibe\s*?Sensor|HapticsSensor_)\s*(.*)$", RegexOptions.IgnoreCase);
      public static Shader Shader;

      public readonly Camera Camera;
      private OrthographicDepth mSampler;

      public TouchSensor(string name, SensorOwnerType ownerType, Camera camera)
        : base(Pattern, name, ownerType)
      {
        Camera = camera;
      }

      public override GameObject GameObject => Camera.gameObject;

      public override bool Enabled {
        get {
          if (Camera != null) {
            return Camera.enabled;
          } else {
            return false;
          }
        }

        set {
          if (Camera != null) {
            Camera.enabled = value;
            if (mSampler != null) {
              mSampler.enabled = value;
            }
          }
        }
      }

      public override float Value {
        get {
          if (mSampler == null) {
            mSampler = Camera.gameObject.AddComponent<OrthographicDepth>();
            mSampler.SetShader(Shader);
          }
          return mSampler.Depth;
        }
      }
    }

    public IEnumerable<Sensor> Sensors => mSensorInstances.Values;
    public int Count => mSensorInstances.Count;
    public event EventHandler<Sensor> SensorDiscovered;
    public event EventHandler<Sensor> SensorLost;

    public static Dictionary<int, TouchSensor> mSensorInstances = new Dictionary<int, TouchSensor>();

    public TouchZoneProvider()
    {
      CVRHooks.AvatarIsReady += OnAvatarIsReady;
    }

    public void Dispose()
    {
      CVRHooks.AvatarIsReady -= OnAvatarIsReady;
    }

    public void OnSceneWasInitialized()
    {
      // In CVR, avatars load before the main scene is initialized.
      // We shouldn't need this as long as OnUpdate removes destroyed sensors :)
      // foreach (var kv in mSensorInstances) {
      //   Util.DebugLog($"TouchZoneProvider: OnSceneWasInitialized: Dropping sensor {kv.Value.Name}");
      //   SensorLost?.Invoke(this, kv.Value);
      // }
      // mSensorInstances.Clear();

      #if DEBUG
        // World sensors (for debugging)
        foreach (var camera in GameObject.FindObjectsOfType<Camera>()) {
          if (IsSensor(camera) && camera.GetComponentInParent<CVRAvatar>() == null) {
            var newSensor = new TouchSensor(camera.name, SensorOwnerType.World, camera); 
            mSensorInstances[camera.GetInstanceID()] = newSensor;
            SensorDiscovered?.Invoke(this, newSensor);
          }
        }
      #endif
    }

    public void OnUpdate()
    {
      // Check for destructions
      var lostSensors = new List<int>();
      foreach (var kv in mSensorInstances) {
        if (kv.Value.Camera == null) {
          lostSensors.Add(kv.Key);
        }
      }
      foreach (var id in lostSensors) {
        var lostSensor = mSensorInstances[id];
        mSensorInstances.Remove(id);
        SensorLost?.Invoke(this, lostSensor);
      }
    }

    private void OnAvatarIsReady(object sender, CVRHooks.AvatarEventArgs args)
    {
      foreach (var camera in args.Avatar.GetComponentsInChildren<Camera>(true)) {
        if (IsSensor(camera)) {
          if (!mSensorInstances.ContainsKey(camera.GetInstanceID())) {
            var newSensor = new TouchSensor(camera.name, args.Avatar == CVRHooks.LocalAvatar ? SensorOwnerType.LocalPlayer : SensorOwnerType.RemotePlayer, camera);
            mSensorInstances[camera.GetInstanceID()] = newSensor; 
            SensorDiscovered?.Invoke(this, newSensor);
          }
        }
      }
    }

    public bool IsSensor(Camera camera)
    {
      return TouchSensor.Pattern.Match(camera.name).Success;
    }

    // private ExpressionParam<float> FindExpressionParam(Camera camera, VRCPlayer player)
    // {
    //   var param = new ExpressionParam<float>(camera.name, player);
    //   return param.Valid ? param : null;
    // }
  }
}
